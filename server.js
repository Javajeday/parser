const express = require('express');
const http = require('http');
const colors = require('colors');
const async = require('async');
const mongoose = require('mongoose');
const Schema = mongoose.Schema;
const request = require('request');
const db = mongoose.connection;
const brands = require('./drom.json');
const fs = require('fs');
const readline = require('readline');
// /* Mongo */
// mongoose.connect('mongodb://localhost/drom', { useNewUrlParser: true });
// db.once('open', () => {
// 	console.log((`MongoDB connected! db name: ${db.name}`).silly);
// });
//
// const brandModel = mongoose.model('brand', new Schema({}));
// brandModel.findOne({ name: 'Audi' }, (error, result) => {
// 	console.log(result.toJSON())
// });
// /* Mongo */

// console.log(brands);
colors.enabled = true;

colors.setTheme({
	silly: 'rainbow',
	input: 'grey',
	verbose: 'cyan',
	prompt: 'grey',
	info: 'green',
	data: 'grey',
	help: 'cyan',
	warn: 'red',
	debug: 'blue',
	error: 'red'
});

let requests = require('./equipRequests.json');
const totalLength = requests.length;
let results = [];
let errors = 0;
let counter = 0;

function makeRequest(index) {
	if (index >= totalLength) {
		saveResult(true, index);
		return;
	}
	if (!(index % 1000)) {
		saveResult(false, index);
	}
	request(requests[index].url,
		{ json: true },
		(err, res, body) => {
			if (err) {
				counter++;
				errors++;
				makeRequest(counter);
			}
			if (body && body.status) {
				counter++;
				results.push({
					...requests[index],
					equipments: body
				})
				readline.clearLine(process.stdout, 0)
				readline.cursorTo(process.stdout, 0, null)
				let text = `${totalLength - 1 - index} left`
				process.stdout.write(text)
				makeRequest(counter);
			} else {
				counter++;
				makeRequest(counter);
			}
		});
}

function saveResult(end, index) {
	fs.writeFile(`equipments_last_${Math.ceil(index / 10000)}.json`, JSON.stringify(results), (err) => {
			if (err) throw err;
			console.log('Saving Complete');
			if (end) {
				process.exit(0);
			}
		}
	);
}
makeRequest(0);
