const mongoose = require('mongoose');
mongoose.connect('mongodb://localhost/drom');
const db = mongoose.connection;

db.once('open', () => {
    console.log((`MongoDB connected! db name: ${db.name}`).silly);
});

module.exports = mongoose;